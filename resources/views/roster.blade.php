
@extends('layouts.app') 
@section('content')

<section>


  <div class="main-container container-fluid">
    <!-- heading -->
    <div class="container-fluid">

      <div >
        <h1 class="text-primary mr-auto">Create Employee Roster</h1>
      </div>
      
      <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myRosterModal">+Create Roster</button><br>

    </div><br>

    <div></div><br>
    <div class="container-fluid">

     <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="em">
      <thead class="thead-dark">
        <tr>
          <th> Serial.no</th>
          <th> Date From</th>
          <th> Date To</th>
          <th> Shift</th>
          <th> Action</th>
        </tr>
      </thead>

      @foreach($rosters as $key=>$roster)
      
      <tbody>
        <tr class="">
          <!-- <td >{{$roster->id}}</td> --> 
          <td>{{$key+1}}</td>
          <td >{{$roster->date_from}}</td> 
          <td >{{$roster->date_to}}</td>
          <td >{{$roster->shift->name}}</td>
          
          <td class="align-middle">

           <a href="{{route('adminview',$roster->id)}}" class="btn btn-primary" >View</a> 

           <form action="{{route('roster_delete',$roster->id)}} " method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger">Delete</button>
          </form>
          
        </td>
      </tr>

    </tbody>

    @endforeach

  </table>

</div>

<!-- /table -->
</div>





<!-- View Button  Modal -->



<!--/Create Roster Button MOdal-->

<div class="modal" id="myRosterModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create Roster</h5>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form action="{{route('roster_post',1)}}" method="post">
          @csrf

          @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach  
            </ul>
          </div>
          @endif

          @if(session()->has('message'))

          <div class="alert alert-{{session('type')}}">
            {{session('message')}}

          </div>

          @endif
          <div>
            <label for="date_from">Date From</label>
            <input type="date" class="form-control"  aria-describedby="inputGroup" name="date_from">

          </div>

          <div>
            <label for="date_from">Date to</label>
            <input type="date" class="form-control"  aria-describedby="inputGroup" name="date_to">

          </div>

          <label>Employee List </label>
          <select class="selectpicker form-control" multiple data-live-search="true" name="emp_id[]">

           @foreach($employees as $employee)
           <option value="{{$employee->id}}">{{$employee->name}}</option>
           @endforeach
         </select>


         <div class="form-group">            
          <label>Shifts:</label>            
          <select class="form-control" name="shift_id"/>
          @foreach($shifts as $shift) 
          <option value="{{$shift->id}}"> {{$shift->name}} </option>
          @endforeach

        </select>       
      </div>         

    </div>
    <button class="btn btn-primary" type="submit" >Submit</button><br>
  </div>

</form>

</div>
</div>

</div>

</div>

</div>



<!--/Roster MOdal-->

</section>


@stop
