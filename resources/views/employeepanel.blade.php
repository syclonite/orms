@extends('layouts.app')

@section('content')

<section>
	
<div class="container register-form">
           	


           	 <div class="form">
                <div class="note">

                    <p>MY PROFILE</p>
                    
                </div>
			
			<form  method="POST" action="{{route('employeepanel_post')}} " role="form" enctype="multipart/form-data">
	
				@csrf
                    
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('message'))

<div class="alert alert-{{session('type')}}">
    {{session('message')}}

</div>

@endif
			
                <div class="form-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Your Name *" value="{{auth()->user()->name}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="fname" class="form-control" placeholder="Fathers Name *" value="{{auth()->user()->fname}}"/>
                            </div>

                            <div class="form-group">
                                <input type="text" name="mname" class="form-control" placeholder="Mothers Name *" value="{{auth()->user()->mname}}"/>
                            </div>

                            <div class="form-group">
                                <input type="number" name="phone" class="form-control" placeholder="Your Phone Number *" value="{{auth()->user()->phone}}"/>
                            </div>
                      </div>

                       
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="username" class="form-control" placeholder=" Username *" value="{{auth()->user()->username}}"/>
                            </div>

                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Your Password  *" value="*****"/>
                            </div>

                            <div class="form-group">
                                <input type="text" name="present" class="form-control" placeholder="Present Address *" value="{{auth()->user()->present}}"/>
                            </div>

                            <div class="form-group">
                                <input type="text" name="permanent" class="form-control" placeholder="Permanent Address *" value="{{auth()->user()->permanent}}"/>
                            </div>
                            
                                                            
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email Address*" value="{{auth()->user()->email}}"/>
                            </div>
                    
           					 </div>
					    </div>
                    <button class="btn btn-warning" type="submit" >Update</button>
                </div>
            
			</form>
</div>
	



</section>

@endsection
