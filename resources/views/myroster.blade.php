@extends('layouts.app')
@section('content')
<section>



  <div class="main-container container-fluid">
    <!-- heading -->
    <div class="container-fluid">
      <div><h1>MY Roster</h1></div>
     
    
    <div class="row">
    <form action="{{route('my_view_get')}}" method="get"> 

      @csrf 
    
      <div class="input-group col-md-12">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup">Date-From</span>
  </div>
  <input type="date" name="date_from" class="form-control" id="date_from" onblur="compare();">
  
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup">Date-To</span>
    </div>
    <input type="date" name="date_to" class="form-control" id="date_to" onblur="compare();">

   <button type="submit" class="btn btn-primary">Submit</button>

  </div>

  </div>

</form>
  </div>
    <!-- /heading -->
    


    <div><br></div>

    <!-- table -->
    <div class="container-fluid">
    <table class="table table-striped table-bordered" id="em" cellspacing="0" width="100%">
      <thead class="thead-dark">
        <tr>
          <th> Serial No. </th>
          <th> Name</th>
          <th> Date</th>
          <th> Shift</th>
          <th> Status</th>
          
          </tr>
      </thead>
      <tbody>
  
        @foreach($roster_details as $key=>$data)
        <tr class="">

          <td >{{$key+1}}</td>
          <td >{{$data->employee->name}}</td>
          <td >{{$data->date}}</td>
          <td >{{$data->shift->name}}</td>
          <td>
        @foreach($holidays as $holiday)
          @if($data->date == $holiday->date)
            Holiday
          @endif
        @endforeach
        </td>
       </tr>
        @endforeach

     </tbody>

   </table>
  
   
</div>

   <!-- /table -->

   <!-- /Attachment Modal For View button-->

  <!-- /Attachment Modal For View button-->



  <script>

    $(document).ready(function(){

      $(document).on('click', "#view-item", function() {
    $(this).addClass('view-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

    var options = {
      'backdrop': 'static'
    };
    $('#view-modal').modal(options)
  })


      $('#view-modal').on('hide.bs.modal', function() {
        $('.view-item-trigger-clicked').removeClass('view-item-trigger-clicked')
        $("#view-form").trigger("reset");
      })
    })



    function compare()
{
    var startDt = document.getElementById("date_from").value;
    var endDt = document.getElementById("date_to").value;

    if( (new Date(startDt).getTime() > new Date(endDt).getTime()))
    {
        alert("Date Is Invalid");

    }
}


  </script>




</section>

@endsection