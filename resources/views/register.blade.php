@extends('layouts.app')

@section('content')

<section>
	
<div class="container register-form">
           	


           	 <div class="form">
                <div class="note">

                    <p>REGISTRATION FORM</p>
                    
                </div>
			
			<form  method="post" action="{{route('registration_post')}} " role="form" enctype="multipart/form-data">
	
				@csrf
                    
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('message'))

<div class="alert alert-{{session('type')}}">
    {{session('message')}}

</div>

@endif


                <div class="form-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Your Name *" value="{{old('name')}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="fname" class="form-control" placeholder="Fathers Name *" value="{{old('fname')}}"/>
                            </div>

                            <div class="form-group">
                                <input type="text" name="mname" class="form-control" placeholder="Mothers Name *" value="{{old('mname')}}"/>
                            </div>

                            <div class="form-group">
                                <input type="number" name="phone" class="form-control" placeholder="Your Phone Number *" value="{{old('phone')}}"/>
                            </div>
                      </div>

                       
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" name="username" class="form-control" placeholder=" Username *" value="{{old('username')}}"/>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Your Password  *" value=""/>
                            </div>

                            <div class="form-group">
                                <input type="text" name="present" class="form-control" placeholder="Present Address *" value="{{old('present')}}"/>
                            </div>

                            <div class="form-group">
                                <input type="text" name="permanent" class="form-control" placeholder="Permanent Address *" value="{{old('permanent')}}"/>
                            </div>
                            
                            <!-- <div class="form-group">
                                <input type="file" name="photo" class="form-control" placeholder="Your Updated Photo" value="{{old('photo')}}" />
                            </div> -->
                                
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email Address*" value="{{old('email')}}"/>
                            </div>
                        
                       
                            <div class="form-group">
                                <input type="float" name="ssc" class="form-control" placeholder="Your SSC result *" value="{{old('ssc')}}"/>
                            </div>

                            <div class="form-group">
                                <input type="float" name="hsc" class="form-control" placeholder="Your HSC result *" value="{{old('hsc')}}"/>
                            </div>        
                             
                           <div class="form-group">
                                <input type="float" name="bsc" class="form-control" placeholder="Your Bsc result *" value="{{old('bsc')}}"/>
                            </div>
                                

                                                            
                            <div class="form-group">
                              <label for="department">Department:</label>
                              <select  class="form-control"  id="department" name="department">
                                    
                                    <option value="none">None</option>
                                    <option value="hr">HR</option>
                                    <option value="sales">Sales</option>
                                    <option value="it">IT</option>
                                    <option value="marketting">Marketting</option>    
                              </select>
                            </div>
                                                 
                            <div class="form-group">
							  <label for="sex">Sex:</label>
							  <select class="form-control" id="sex" name="sex">
									
									<option value="none">None</option>
								    <option value="male">Male</option>
								    <option value="female">Female</option>
								    
							  </select>
							</div>
                            
                            <div class="form-group">
                              <label for="designation">Designation:</label>
                              <select class="form-control" id="designation" name="designation">
                                    
                                    <option value="none">None</option>
                                    <option value="admin">Admin</option>
                                    <option value="employee">Employee</option>
                                    <option value="manager">Manager</option>
                                    <option value="leader">Team Leader</option>

                              </select>
                            </div>
                        
                        </div>                 


                    </div>
                    <button class="btn btn-primary" type="submit" >Submit</button>
                </div>
            </div>
			</form>
</div>
	



</section>

@endsection
