@extends('layouts.app')
@section('content')
<section>

	<div class="main-container container-fluid">
		<!-- heading -->
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="text-primary mr-auto">Shift Setup</h1>
				</div>
			</div>
		</div>
		<!-- /heading -->
		<div><br></div>
		<!-- table -->

		<div class="container-fluid">

			<div>
				<button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myShiftModal">+Add Shift</button>
			</div><br>

			<table class="table table-striped table-bordered" id="em" cellspacing="0" width="100%">
				<thead class="thead-dark">
					<tr>
						<th> Serial.no</th>
						<th> Time Start</th>
						<th> Time End</th>
						<th> Working Hour</th>
						<th> Shift Name</th>
						<th> Action</th>
					</tr>
				</thead>
					
					

				<tbody>
						
						@foreach($data as $key=>$shift)    
						<tr class="data-row">
						<td >{{$key+1}}</td>
						<td >{{date('g:i a',strtotime($shift->schedule->timestart))}}</td>
						<td >{{date('g:i a',strtotime($shift->schedule->timeend))}}</td>
						<td>{{date('g:i',strtotime($shift->schedule->timeend)-strtotime($shift->schedule->timestart))}} Hours</td>
						<td >{{$shift->name}}</td>
						<td class="align-middle">
							<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myShiftEditModal{{$shift->id}}">Edit</button>
							<form action="{{route('shift_delete',$shift->id)}}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button type="submit" class="btn btn-danger">Delete</button>
							</form>
						 </td>
						</tr>
						
					<!--Edit Shift Details -->

					<div class="modal" id="myShiftEditModal{{$shift->id}}">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Update Shift</h5>
									<button class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<form action="{{route('shift_update',$shift->id)}}" method="POST">

										@csrf

										<div class="form-group">
											<label >Shift Name</label>
											<input class="form-control" type="text" placeholder="Shift Name" name="shift_name" value="{{$shift->name}}">
										</div>

											<div class="form-group">
											<label>Time Start </label>
											<input class="form-control" type="time" name="time_start" value="{{$shift->schedule->timestart}}">
										</div>

										<div class="form-group">
											<label>Time End </label>
											<input class="form-control" type="time" name="time_end" value="{{$shift->schedule->timeend}}">
										</div>
									
									<div class="modal-footer">
										<button class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button class="btn btn-warning" type="submit">Save</button>
									</div>	  
								</form>
										
							</div>

						</div>
					</div>
				</div>
			</div>
		
	<!--Edit Shift Details -->
	@endforeach	
	</tbody>
	
		
	</table>
	<!--Add Holiday Details -->
	<div class="modal" id="myShiftModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Shift</h5>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="{{route('shift_post')}}" method="POST">

						@csrf
						

						@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif

						@if(session()->has('message'))

						<div class="alert alert-{{session('type')}}">
							{{session('message')}}

						</div>

						@endif


						<div class="form-group">
							<label >Shift Name</label>
							<input class="form-control" type="text" placeholder="Shift Name" name="shift_name">
						</div>
						<div class="form-group">
							<label>Start Time</label>
							<input class="form-control" type="time" name="time_start" >
						</div>
						<div class="form-group">
							<label>End Time</label>
							<input class="form-control" type="time" name="time_end" >
						</div>
						
						<div class="modal-footer">
							<button class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button class="btn btn-warning" type="submit">Save</button>
						</div>	  
					</form>
				</div>

			</div>
		</div>
	</div>
</div>
<!--Add Holiday Details -->





</div>

<!-- /table -->
</div>	


</section>

@endsection