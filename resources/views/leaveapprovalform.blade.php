@php
 
use App\Models\leave_total;

@endphp

@extends('layouts.app')
@section('content')

<section>


  <div class="main-container container-fluid">
    <!-- heading -->
    <div class="container-fluid">
      <div><h1>Leave Approval Form</h1></div>
    
    
    <div class="row">
    
      <div class="input-group col-md-6">
    <!-- <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup">Date-From</span>
  </div>
  <input type="date" class="form-control" aria-label="Date-From" 
  aria-describedby="inputGroup">
  

    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup">Date-To</span>
    </div>
    <input type="date" class="form-control" aria-label="Date-To" aria-describedby="inputGroup">

   <button type="submit" class="btn btn-primary">Submit</button>

  </div> -->

  </div>

  </div>
  <div><br></div>
    <!-- table -->
    <div class="container-fluid">
      <table class="table table-striped table-bordered" id="em" cellspacing="0" width="100%">
        <thead class="thead-dark">
          <tr>
            <th> Emp_id</th>
            <th> Name</th>
            <th> Date-from</th>
            <th> Date-to</th>
            <th> Leave Duration</th>   
            <th> Remaning Leave</th>         
            <th> Leave Type</th>
            <th> Reason</th>
            <th> Emergency Contact</th>
            <th> Locations</th>
            <th> Status</th>
            <th> Action</th>
            </tr>
        </thead>

  @foreach($data as $user)

  @php
$remaining=Leave_total::select('remaning_leave')->where('emp_id',$user->emp_id)->first();

  @endphp
        <tbody>
          <tr class="data-row">
            <td >{{$user->emp_id}}</td>
            <td >{{$user->employee->name}}</td>
            <td >{{$user->date_from}}</td>
            <td >{{$user->date_to}}</td>
            <td >{{$user->duration}}</td> 
            <td>{{$remaining ? $remaining->remaning_leave : '0'}}</td>
            <td >{{$user->leave_type}}</td>
            <td >{{$user->reason}}</td>
            <td >{{$user->urgent_contact}}</td>
            <td >{{$user->location}}</td>
            <td >{{$user->status}}</td>
            <td class="align-middle">
                @csrf
        @if($user->status == 'pending') 

        <a href="{{route('leave_update_get',$user->id)}}" class="btn btn-primary ">Approve</a>
        
        @if($user->status == 'approved')
  
          
        <a href="{{route('leave_update_get',$user->id)}}" class="btn btn-primary disabled ">Approve</a>
        @endif
  
        
              
        <a href="{{route('leave_reject_get',$user->id)}}" class="btn btn-danger ">Reject</a>
                
        @if($user->status == 'reject')
  
          
        <a href="{{route('leave_reject_get',$user->id)}}" class="btn btn-danger disabled ">Reject</a>
        @endif

        @endif


            </td>            

          </tr>
          </tbody>
            @endforeach

  </table>
</div>
<!-- /table -->

  <script>
 

  

  </script>




</section>



@endsection