@php
 
use App\Models\leave_total;

@endphp

@extends('layouts.app')
@section('content')

<section>


  <div class="main-container container-fluid">
    <!-- heading -->
    <div class="container-fluid">
      <div><h1>My Leave Status</h1></div>
    
    
    <div class="row">
    
      <div class="input-group col-md-6">
  

  </div>

  </div>
  <div><br></div>
    <!-- table -->
    <div class="container-fluid">
      <table class="table table-striped table-bordered" id="em" cellspacing="0" width="100%">
        <thead class="thead-dark">
          <tr>
            <th> Name</th>
            <th> Date-from</th>
            <th> Date-to</th>
            <th> Leave Duration</th>        
            <th> Leave Type</th>
            <th> Reason</th>
            <th> Emergency Contact</th>
            <th> Locations</th>
            <th> Status</th>
            
            </tr>
        </thead>


        <tbody>

      @foreach($data as $status)
          <tr class="data-row">
            <td >{{$status->employee->name}}</td>
            <td >{{$status->date_from}}</td>
            <td >{{$status->date_to}}</td>
            <td >{{$status->duration}}</td>
            <td>{{$status->leave_type}}</td>
            <td >{{$status->reason}}</td>
            <td >{{$status->urgent_contact}}</td>
            <td >{{$status->location}}</td>
            <td >{{$status->status}}</td>
            
            
          </tr>
      @endforeach
          </tbody>

  </table>
</div>
<!-- /table -->

  <script>
 

  

  </script>




</section>



@endsection