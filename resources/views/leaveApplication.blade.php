@extends('layouts.app')
@section('content')

<section>


  <div class="container leave-application-form">           


   <div class="form">
    <div class="note">

      <p>Leave Application Form</p> 
    </div>

    <form method="POST" action="{{route('storeapplication_post')}}" role="form">
  
      @csrf

      
      @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('message'))

<div class="alert alert-{{session('type')}}">
    {{session('message')}}

</div>

@endif
      
      <div class="form-content">
        <div class="row">
                      
         <div class="col-md-8">
          
          <div class="form-group">
            <input type="number" name="emg_contact" class="form-control" placeholder="Emergency contact  *" value="{{old('emg_contact')}}"/>
          </div>
          <div class="form-group">
            <input type="text" name="location" class="form-control" placeholder="Vacation location" value="{{old('location')}}"/>
          </div>
          <div class="form-group">
            <input type="text" name="reason" class="form-control" placeholder="Reason For Leave  *" value="{{old('reason')}}"/>
          </div>
          
          <div class="form-group">
                              <label for="leave_type">Leave Type:</label>
                              <select  class="form-control" name="leave_type">
                                    
                                    <option value="none">None</option>
                                    <option value="casual">Casual</option>
                                    <option value="medical">Medical</option>
                                    <option value="annual">Earned Leave</option>    
                              </select>
                            </div>

          <div class="row">

            <div class="input-group col-md-12">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup">Date-From</span>
              </div>
              <input type="date" class="form-control" name="date_from" aria-describedby="inputGroup">


              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup">Date-To</span>
              </div>
              <input type="date" class="form-control" name="date_to" aria-describedby="inputGroup">

            </div>

          </div>

        </div>        

      </div>
      <div><br></div>
      <button type="submit" class="btn btn-primary btn-lg">SUBMIT</button>


    </div>
  </form>
</div>





</section>



@endsection