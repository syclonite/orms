@extends('layouts.app')
@section('content')

<section>

	<div class="main-container container-fluid">
		<!-- heading -->
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="text-primary mr-auto">Holiday Setup</h1>
				</div>
			</div>
		</div>
		<!-- /heading -->
		<div><br></div>
		<!-- table -->

		<div class="container-fluid">

			<div>
				<button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myHolidayModal">+Add Holidays</button>
			</div><br>

			<table class="table table-striped table-bordered" id="em" cellspacing="0" width="100%">
				<thead class="thead-dark">
					<tr>
						<th> Serial.no</th>
						<th> Holiday Name</th>
						<th> Date</th>
						<!-- <th> To Date</th> -->
						<th> Discriptions</th>
						<th> Status</th>
						<th> Action</th>
					</tr>
				</thead>

				<tbody>    

					@foreach($holidays as $holiday)
					<tr class="data-row">
						<td>{{$holiday->id}}</td>
						<td >{{$holiday->holiday_name}}</td>
						<td >{{$holiday->date}}</td>
						<!-- <td >{{$holiday->date_to}}</td> -->
						<td >{{$holiday->discription}}</td>
						<td >{{$holiday->status}}</td>

						<td class="align-middle">
							<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myHolidayEditModal{{$holiday->id}}">Edit</button>
							<form action="{{route('holiday_delete',$holiday->id)}} " method="POST">
								{{ csrf_field() }}
            				{{ method_field('DELETE') }}
							<button type="submit" class="btn btn-danger">Delete</button>
						</form>
						</td>
					</tr>


					<!--Edit Holiday Details -->

					<div class="modal" id="myHolidayEditModal{{$holiday->id}}">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Update Holidays</h5>
									<button class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<form action="{{route('holiday_update',$holiday->id)}}" method="POST">

										@csrf

										<div class="form-group">
											<label >Holiday Name</label>
											<input class="form-control" type="text" placeholder="Holiday Name" name="name" value="{{$holiday->holiday_name}}">
										</div>

										<div class="form-group">
											<label>date From </label>
											<input class="form-control" type="date" name="date" value="{{$holiday->date}}">
										</div>

										<div class="form-group">            
											<label >Discription</label>            
											<input class="form-control" type="text" name="discription" placeholder="Discription" value="{{$holiday->discription}}">      
										</div>

									<!-- 	<div class="form-group">            
											<label>Status</label>            
											<select class="form-control" name="status"/>
											<option value="deactive">Deactive</option>

										</select>       
									</div>		
 -->
									<div class="modal-footer">
										<button class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button class="btn btn-warning" type="submit">Save</button>
									</div>	  
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
			<!--Edit Holiday Details -->

			@endforeach

		</tbody>

	</table>
	<!--Add Holiday Details -->
	<div class="modal" id="myHolidayModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Add Holidays</h5>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="{{route('holiday_store')}}" method="POST">

						@csrf

						<div class="form-group">
							<label >Holiday Name</label>
							<input class="form-control" type="text" placeholder="Holiday Name" name="name">
						</div>
						<div class="form-group">
							<label>date From </label>
							<input class="form-control" type="date" name="date_from">
						</div>
						<div class="form-group">            
							<label >Date To</label>            
							<input class="form-control" type="date" name="date_to">
						</div>
						<div class="form-group">            
							<label >Discription</label>            
							<input class="form-control" type="text" name="discription" placeholder="Discription"/>       
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button class="btn btn-warning" type="submit">Save</button>
						</div>	  
					</form>
				</div>

			</div>
		</div>
	</div>
</div>
<!--Add Holiday Details -->





</div>

<!-- /table -->
</div>

</section>

@endsection