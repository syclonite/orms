@extends('layouts.app')
@section('content')
<section>



  <div class="main-container container-fluid">
    <!-- heading -->
    <div class="container-fluid">
      <div><h1>Report</h1></div>
     
    
    <div class="row">
    <form action="{{route('report_get')}}" method="get"> 

      @csrf 
    
      <div class="input-group col-md-12">
    <div class="input-group-prepend">
    <span class="input-group-text" >Date-From</span>
  </div>
  <input type="date" name="date_from" class="form-control" id="date_from" onblur="compare();">
  
    <div class="input-group-prepend">
      <span class="input-group-text" >Date-To</span>
    </div>
    <input type="date" name="date_to"  class="form-control" id="date_to"  onblur="compare();">

   

   <button type="submit" class="btn btn-primary">Submit</button>
	  
	<div class="">
		
   <button  onClick="printData()" id="print"	class="btn btn-warning" >Print</button> 

    </div>
 
  </div>

    </div>

</form>
  </div>
    <!-- /heading -->



    <div><br></div>

    <!-- table -->
    <div class="container-fluid" >
    <table class="table table-striped table-bordered" id="em" cellspacing="0" border="1"  width="100%">
      <thead class="thead-dark">
        <tr>
          <th> Serial No. </th>
          <th> Name</th>
          <th> Date</th>
          <th> Shift</th>
		</tr>
      </thead>
      <tbody>
  
        @foreach($roster_details as $key=>$data)
        <tr class="">

          <td >{{$key+1}}</td>
          <td >{{$data->employee->name}}</td>
          <td >{{$data->date}}</td>
          <td >{{$data->shift->name}}</td>
          </tr>
        @endforeach

     </tbody>

   </table>
  
</div>

<script>
	
function printData()
{
   var divToPrint=document.getElementById("em");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}

$('#print').on('click',function(){
printData();
})


function compare()
{
    var startDt = document.getElementById("date_from").value;
    var endDt = document.getElementById("date_to").value;

    if( (new Date(startDt).getTime() > new Date(endDt).getTime()))
    {
        alert("Date Is Invalid");

    }
}


</script>

</section>

@endsection