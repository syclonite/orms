@extends('layouts.app')


@section('content')

          <div class="container"> <br>

                  <form action="" method="">
                    @csrf
                      
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="em">
                      <thead class="thead-dark">
                        <tr>
                         <!-- <th>Employee Id</th> -->
                          <th>Employee Name</th> 
                          <th>Date</th>
                          <th>Shift</th>
                          <th>Action</th>
                          </tr>

                      </thead>
                      
                  <tbody>
                    @foreach($roster_details as $data)
                    <tr>
                      
                     <!-- <td>{{$data->emp_id}}</td> -->

                      <td>{{$data->employee->name}}</td>
                      <td>{{$data->date}}</td>
                       <td>{{$data->shift->name}}</td> 

                        <td class="align-middle">
            <a href="" class="btn btn-success" data-toggle="modal" data-target=" #myEditModal{{$data->id}}">Edit</a>

                                
          <form action="{{route('roster_details_delete',$data->id)}}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-danger">Delete</button>
            </form>

                
                <div class="modal" id="myEditModal{{$data->id}}">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Edit Employee</h5>
                  <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <form action="{{route('roster_edit',$data->id)}}" method="post">
                    @csrf

                   <div class="form-group">
                      <label for="name">Name</label>
                      <input class="form-control" type="text" name="name" value="{{$data->employee->name}}">
                    </div>
                    
                    <div class="form-group">            
                    <label>Shifts:</label>

                    <select class="form-control" name="shift_id"/>
                    @foreach($shifts as $shift)
                    <option
                     @if($shift->id==$data->shift_id)
                      selected 
                      @endif value="{{$shift->id}}">{{$shift->name}}</option>
                    @endforeach
                  </select>   

                </div> 

                    <div class="form-group">            
                      <label for="date_from">Date</label>            
                      <input class="form-control" type="date" name="date" value="{{$data->date}}">   
                          
                  <div class="modal-footer">
                  <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button class="btn btn-warning" type="submit">Update</button>
                </div>

              </form>

            </div>
          </div>

        </div>

      </div>

    </div>

              
          </td>                                  
          </tr>
                @endforeach
                    

                  </tbody>

                      </table>
       
                      
                                   
              </form>

            
</div>

@endsection
