@extends('layouts.app')
@section('content')
<section>



  <div class="main-container container-fluid">
    <!-- heading -->
    <div class="container-fluid">
      <div><h1>Employee Profile</h1></div>
     
    
     
   <div>
        <a href="{{url('/register')}}" class="btn btn-info btn-lg"  >Add New Employee</a>
      </div>

    <!-- /heading -->
    


    <div><br></div>

    <!-- table -->
    <div class="container-fluid">
    <table class="table table-striped table-bordered" id="em" cellspacing="0" width="100%">
      
      <thead class="thead-dark">
        <tr>
         <th> Serial No.</th>
          <th> Id</th>
          <th> Name</th>
          <th> Phone</th>
          <th> Email</th>
          <th> Username</th>
          <th> Department</th>
          <th> Designation</th>
          <th> Fathers Name</th>
          <th> Mothers Name</th>
          <th> Present Address</th>
          <th> Permanent Address</th>
          <th> Sex </th>
          <th> SSC Result</th>
          <th> HSC Result</th>
          <th> BSC Result</th>
          <th> Action</th>
          
          </tr>
      </thead>
      <tbody>
  
        @foreach ($employees as  $key=>$employee)

        <tr class="">
          <td>{{$key+1}}</td>
          <td>{{$employee->id}}</td>
          <td>{{$employee->name}}</td>
          <td>{{$employee->phone}}</td>
          <td>{{$employee->email}}</td>
          <td>{{$employee->username}}</td>
          <td>{{$employee->department}}</td>
          <td>{{$employee->designation}}</td>
          <td>{{$employee->fname}}</td>
          <td>{{$employee->mname}}</td>
          <td>{{$employee->present}}</td>
          <td>{{$employee->permanent}}</td>       
          <td>{{$employee->sex}}</td>
          <td>{{$employee->ssc}}</td>
          <td>{{$employee->hsc}}</td>
          <td>{{$employee->bsc}}</td>


          <td class="align-middle">
            <a href="{{ route('employee_edit',$employee->id) }}" class="btn btn-success" data-toggle="modal" data-target="#myEditModal{{ $employee->id }}">Edit</a> 

            <form method="POST" action="{{ route('employee_profile_delete',$employee->id) }}">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-danger">Delete</button>
            </form>

            <div class="modal" id="myEditModal{{ $employee->id }}">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Edit Employee</h5>
                    <button class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <form action="{{route('employee_profile_update',$employee->id)}}" method="post">
                      @csrf


                      <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" type="text" name="name" value="{{$employee->name}}">
                      </div>

                      <div class="form-group">
                        <label for="phone">Phone</label>
                        <input class="form-control" type="phone" name="phone" value="{{$employee->phone}}">
                      </div>

                      <div class="form-group">            
                        <label for="email">Email:</label>            
                        <input class="form-control" type="email" name="email" value="{{$employee->email}}">       
                      </div>


                      <div class="form-group">            
                        <label for="username">Username:</label>            
                        <input class="form-control" type="text" name="username" value="{{$employee->username}}">       
                      </div>

                      <div class="form-group">            
                        <label>Department:</label>            
                        <select class="form-control" name="department"/>
                        <option >None</option>
                        <option value="hr">HR</option>
                        <option value="sales">Sales</option>
                        <option value="it">IT</option>
                        <option value="marketting">Marketting</option>            
                      </select>       

                    </div>

                    <div class="form-group">            
                      <label>Designation:</label>            
                      <select class="form-control" name="designation"/>
                      <option >None</option>
                      <option value="admin">Admin</option>
                      <option value="employee">Employee</option>
                      <option value="manager">Manager</option>
                      <option value="leader">Team Leader</option>

                    </select>       
                  </div>

                  <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-warning" type="submit">Update</button>
                  </div>

                </form>

              </div>
            </div>

          </div>

        </div>

      </div>


    </td>
  </tr>

  @endforeach
  
   
</div>


</section>

@endsection