 @extends('layouts.app')

@section('content')

<section>

    
    <div class="container login-form">
       
       <div class="form">
        <div class="note">

            <p>LOGIN FORM</p> 
        </div>


        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif


        <form method="POST" action="{{route('login_post')}}" >
            
           @csrf

           @if(session()->has('message'))

            <div class="alert alert-danger">
                {{session('message')}}

            </div>

            @endif 


            <div class="form-content">
                <div class="row">
                    
                   <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder=" Username *" value=""/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Your Password  *" value=""/>
                    </div>
                </div>
                
            </div>
            
            <button class="btn btn-primary" type="submit" >Submit</button>

        </div>
        
    </div>
</div>
</form>
</div>

</section>

@endsection
