@extends('layouts.app')

@section('content')
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
@if ($errors->any())
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

      @if(session()->has('message'))

            <div class="alert alert-success">
                {{session('message')}}

            </div>

            @endif 

            
<section class="sec1"> 

	<h1> <div>Online Roster Management System</div> </h1>
	<div id="particles-js"> </div>
	
          <script src="js/particles.js"></script>
			
          <script src="js/app.js"></script> 	

</section>
			


@endsection