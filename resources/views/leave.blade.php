@extends('layouts.app')
@section('content')

<section>
  

<div class="main-container container-fluid">
  <!-- heading -->
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <h1 class="text-primary mr-auto">Employee Leave Status</h1>
      </div>
    </div>
  </div>
  <!-- /heading -->
<div><br></div>
  <!-- table -->
  <div class="container-fluid">

    <table class="table table-striped table-bordered" id="em" cellspacing="0" width="100%">
      <thead class="thead-dark">
        <tr>
          <th> Serial.no</th>
          <th> Name</th>
          <th> Total Leave</th>
          <th> Remaining Leave</th>
          <!-- <th> Action</th> -->
        </tr>
      </thead>

      <tbody>
     
        @foreach($total_leave as $key=>$data)
        <tr class="data-row">
          <td >{{$key+1}}</td>
          <td >{{$data->employee->name}}</td>
          <td >{{$data->total_leave}}</td>
          <td >{{$data->remaning_leave}}</td>
          <!-- <td class="align-middle">
            <button type="button" class="btn btn-success" id="edit-item" data-item-id="1">edit</button>
          </td> -->
        </tr>
     @endforeach        
      </tbody>

    </table>

  </div>

  <!-- /table -->
</div>

<!-- /Attachment Modal -->




<!--/Roster MOdal-->


<script>
  



</script>



</section>

@endsection