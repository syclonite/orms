     <div class="navbar-inner">
      <nav >
        <div class="logo">
          <img src="images/index.png">  
        </div>

        <div class="toggle">

          <div class=" menu"> <i class="fa fa-bars"  aria-hidden="true"></i></div>

        </div>

        <ul>
         <!--  <li><a href="{{route('home')}}">Home</a></li> -->
          @auth()
          @if(auth()->user()->designation=='admin'  || auth()->user()->designation=='leader' || auth()->user()->designation=='hr' ||auth()->user()->designation=='manager')
          <li><a href="{{route('profile')}}" class="disabled">{{auth()->user()->name}}</a></li>
          
            <li><a href="{{route('holiday_get')}}">Holiday</a></li>
          
          
          <li> <a href="{{route('shift_get')}}" class="">Shift</a></li>
          <li> <a href="#" class="disabled">Employee</a>
            <ul>  

              <li><a href="{{route('employee_profile_get')}}">Employee List</a></li>
              
            </ul>
          </li>
        
          <li>
            <a href="{{route('registration_get')}}">
            Sign Up</a>
            
          </li>
          <li><a href="{{route('logout')}}">Sign out</a></li>  
              @endif
          @endauth


          @guest()
          <li><a href="{{route('login')}}">Sign In</a></li> 
          @endguest

     @auth()
        @if(auth()->user()->designation=='admin'  || auth()->user()->designation=='leader' || auth()->user()->designation=='hr' ||auth()->user()->designation=='manager')
          <li><a href="#" class="disabled">Roster</a>
           <ul>
            
            <li><a href="{{route('roster_get')}}">Create Roster</a></li> 
            <li><a href="{{route('employee_view_roster_get')}}">View Roster</a></li>
            
            <li><a href="{{route('my_view_get')}}">My Roster</a></li>
          </ul>
        </li>

        <li><a href="" class="disabled">Leave</a>

         <ul>
          
          <li><a href="{{route('leaveApplication_get')}}">Leave Application Form</a></li> 
          <li><a href="{{route('leave_approval_get')}}">Leave Approval</a></li>
          <li><a href="{{route('emp_leave_status_get')}}">Employee Leave Status</a></li>
          
        </ul>
      </li>
        <li><a href="{{route('report_get')}}" >Report</a></li>

        @endif

          @endauth()
      </li>             

    </ul>

<div class="logo">
          <img src="images/index.png">  
        </div>

        <div class="toggle">

          <div class=" menu"> <i class="fa fa-bars"  aria-hidden="true"></i></div>

        </div>

        <ul>
          <li><a href="{{route('home')}}">Home</a></li>
          @auth()
          @if(auth()->user()->designation=='employee')
       <li><a href="{{route('profile')}}" class="disabled">{{auth()->user()->name}}</a>
            
            <ul>
              
              <li><a href="{{route('employeepanel_get')}}">My Profile</a></li>
            </ul>
        
       </li>
         

          <li><a href="{{route('logout')}}">Sign out</a></li>  
              @endif
          @endauth


         

     @auth()
      @if(auth()->user()->designation=='employee')
          <li><a href="#" class="disabled">Roster</a>
           <ul>          
            <li><a href="{{route('my_view_get')}}">My Roster</a></li>
          </ul>
        </li>

        <li><a href="" class="disabled">Leave</a>

         <ul>
          
          <li><a href="{{route('leaveApplication_get')}}">Leave Application Form</a></li> 
          <li><a href="{{route('mystatus_get')}}">My Leave Status</a></li>
        </ul>
      </li>
      
          @endif

          @endauth()
      </li>             

    </ul>

  





  </nav>

 

</div>




