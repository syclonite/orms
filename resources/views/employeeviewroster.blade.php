@extends('layouts.app')
@section('content')



<!--datatable -->


<div class="main-container container-fluid">
  <!-- heading -->
  <div class="container-fluid">

    <div><h1>Employee view Roster</h1></div>
          
    <div class="row">
      <div class="input-group col-md-4">
    
    
    </div>
  

    </div>

    </div>
  



  <div><br></div>

<div class="container-fluid">
  <!-- table -->
  <table class="table" id="em" cellspacing="0" width="100%">
    <thead class="thead-dark" >
      <tr>
        
        <th> Name</th>
        <th> Date</th>
        <th> Status</th>
        </tr>
    </thead>
  
    <tbody>
      @foreach($roster_details as $data)
      <tr class="">
        <td >{{$data->employee->name}}</td>
        <td >{{$data->date}}</td>
        <td>
        @foreach($holidays as $holiday)
          @if($data->date == $holiday->date)
            Holiday
          @endif
        @endforeach
        </td>
      </tr>
      @endforeach   
    </tbody>
   
  </table>

<!--  -->


@endsection







