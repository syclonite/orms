<?php

/*
|--------------------------------------------------------------------------
| Web Routes


|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

Route::get('/login','fontend\LoginController@create')->name ('login');

Route::post('/login','fontend\LoginController@login')->name ('login_post');


Route::get('/', function () {
	return view('home');
})->name('home');


Route::group(['middleware' =>'auth'], function () {

//Registration////////////////////////////////////////////////////////////////

	Route::get('/register','fontend\EmployeesController@create')->name ('registration_get');
	Route::post('/register','fontend\EmployeesController@store')->name ('registration_post');
	Route::get('/employeeprofile','fontend\EmployeesController@showall')->name('employee_profile_get');

//Route::get('/employee_edit/{id}','fontend\EmployeesController@showEdit')->name('employee_edit');

	Route::get('/employeeprofile/{id}','fontend\EmployeesController@showEdit')->name('employee_edit');

	Route::post('/employeeprofile/{id}','fontend\EmployeesController@update')->name ('employee_profile_update');

	Route::delete('/employeeprofile/{id}','fontend\EmployeesController@destroy')->name('employee_profile_delete');


	Route::get('/employeepanel','fontend\EmployeesController@employeepanel')->name('employeepanel_get');

	Route::post('/employeepanel/update','fontend\EmployeesController@update_employeepanel')->name('employeepanel_post');

//Registration//////////////////////////////////////////////////////////////


// Roster_Details///////////////////////////////////////////////////////////

	Route::get('/roster','fontend\RosterDetailsController@create')->name('roster_get');


	Route::post('/roster','fontend\RosterDetailsController@store')->name('roster_post');

	Route::get('/adminview/{id}','fontend\RosterDetailsController@adminview')->name('adminview');

	Route::post('/adminview/{id}','fontend\RosterDetailsController@updateRosterDetails')->name('roster_edit');

	Route::get('/myroster','fontend\RosterDetailsController@myroster')->name('my_view_get');


	Route::get('/employeeviewroster','fontend\RosterDetailsController@empviewroster')->name('employee_view_roster_get');


	Route::delete('/roster_delete/{id}','fontend\RosterDetailsController@deleteRoster')->name('roster_delete');

	Route::delete('/roster_details_delete/{id}','fontend\RosterDetailsController@deleteRosterDetails')->name('roster_details_delete');


   //Route::post('/myroster','fontend\RosterDetailsController@datefilter')->name('my_view_get');


 //Roster_Details//////////////////////////////////////////////////////////////



//All type of leave Leave///////////////////////////////////////////////////////


	Route::get('/leave','fontend\TotalLeaveController@create')->name('emp_leave_status_get');

	Route::get('/leaveApplication','fontend\LeaveTakenController@leaveapplication')->name('leaveApplication_get');

	Route::post('/leaveApplication','fontend\LeaveTakenController@storeapplication')->name('storeapplication_post');

	Route::get('/leaveapprovalform','fontend\LeaveTakenController@showapplication')->name('leave_approval_get');

	Route::get('/leave_update/{id}','fontend\TotalLeaveController@leavecalc')->name('leave_update_get');
	
	Route::get('/leave_reject/{id}','fontend\TotalLeaveController@leavereject')->name('leave_reject_get');

	Route::get('/myleavestatus','fontend\LeaveTakenController@myleavestatus')->name('mystatus_get');

 

//All type of leave Leave//////////////////////////////////////////////////////

////holiday///////////////////////////////////////////////////////////////////
		Route::get('/holiday','fontend\HolidayController@create')->name('holiday_get');
		Route::post('/holiday/store','fontend\HolidayController@store')->name('holiday_store');
		Route::post('/holiday/update/{id}','fontend\HolidayController@update')->name('holiday_update');
		Route::delete('/holiday/delete/{id}','fontend\HolidayController@destroy')->name('holiday_delete');

////holiday///////////////////////////////////////////////////////////////////
////Shift///////////////////////////////////////////////////////////////////

Route::get('/shift','fontend\ShiftController@show')->name('shift_get');
Route::post('/shift/create','fontend\ShiftController@store')->name('shift_post');
Route::post('/shift/update/{id}','fontend\ShiftController@update')->name('shift_update');
Route::delete('/shift/delete/{id}','fontend\ShiftController@destroy')->name('shift_delete');

////Shift///////////////////////////////////////////////////////////////////

////Report///////////////////////////////////////////////////////////////////
	
	Route::get('/report','fontend\ReportController@show')->name('report_get');

////Report///////////////////////////////////////////////////////////////////

Route::get('/profile','fontend\LoginController@showprofile')->name('profile');

Route::get('/logout','fontend\LoginController@logout')->name('logout');
});
