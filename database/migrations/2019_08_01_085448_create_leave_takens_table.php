<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveTakensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('leave_takens', function (Blueprint $table) {
            $table->Increments('id');
            $table->Integer('emp_id')->unsigned()->nullable();
            $table->date('date_from');
            $table->date('date_to');
            $table->string('duration')->nullable();
            $table->string('location')->nullable();
            $table->string('urgent_contact');
            $table->string('leave_type')->nullable();
            $table->string('status')->default('pending');
            $table->string('approved_by')->nullable();
            $table->string('reason')->nullable(false);
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave__takens');
    }
}
