<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return 

     intr =
     */
    public function up()
    {   
        Schema::create('employees', function (Blueprint $table) {
           
            $table->Increments('id')->unsiged()->nulable();
            $table->string('name',64)->nullable();
            $table->string('phone',64)->unique();
            $table->string('present',60)->nullable();
            $table->string('permanent',60)->nullable();
            $table->string('fname',60)->nullable();
            $table->string('mname',60)->nullable();
            $table->string('email',60)->unique();
            $table->string('username',64)->unique();
            $table->string('password',64)->bcrypt();
            $table->string('sex',7)->nullable();
            $table->string('department',30)->nullable();
            $table->string('designation',30)->nullable(); 
            $table->float('ssc',30)->nullable(); 
            $table->float('hsc',30)->nullable(); 
            $table->float('bsc',30)->nullable(); 
            $table->rememberToken();
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
