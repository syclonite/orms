<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   Schema::enableForeignKeyConstraints();
        Schema::create('rosters', function (Blueprint $table) {
            $table->Increments('id')->unsigned();
            $table->integer('shift_id')->unsigned()->nullable();
            //$table->integer('schedule_id')->unsigned()->nullable();
            $table->string('date_from')->nullable();
            $table->string('date_to')->nullable();
            $table->rememberToken();
            $table->timestamps();
            
            
            $table->foreign('shift_id')->references('id')->on('shifts')->onDelete('cascade');
          



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rosters');
    }
}

