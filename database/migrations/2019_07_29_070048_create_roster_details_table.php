<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRosterDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   Schema::enableForeignKeyConstraints();
        Schema::create('roster_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('roster_id')->unsigned()->nullable();
            $table->integer('shift_id')->unsigned()->nullable();
            $table->integer('emp_id')->unsigned();
            $table->string('date');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('cascade');
            $table->foreign('shift_id')->references('id')->on('shifts')->onDelete('cascade');   
            $table->foreign('roster_id')->references('id')->on('rosters')->onDelete('cascade');   


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roster_details');
    }
}
