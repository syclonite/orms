<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {            
        Schema::create('leave_totals', function (Blueprint $table) {

            $table->Increments('id');
            $table->Integer('emp_id')->unsigned();
            $table->integer('total_leave')->nullable();
            $table->integer('remaning_leave');
            $table->timestamps();
            $table->rememberToken();
            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('cascade');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_totals');
    }
}
