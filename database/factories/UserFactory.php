<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Models\Employee;
use App\Models\Schedule;
use App\Models\Shifts;
use App\Models\Roster;
use App\Models\Leave_total;

use Carbon\Carbon;
/* 
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Employee::class, function (Faker $faker) {
	

    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'username' =>$faker->username,
        'password' => bcrypt('123456'), // password
        'phone' =>$faker->phoneNumber,
        'sex'=>'male',
        'department' =>'it',
        'designation'=>$faker->randomElement(['teamleader','hr','employee','admin']),
        'remember_token' => Str::random(50),
    ];
});
//php artisan make:factory





// $factory->define(Roster::class,function(Faker $faker){

// return [
//         'emp_id'=> random_int(1, 50),
//         'shift_id'=> random_int(1, 2),
//        // 'schedule_id'=> random_int(1, 2),
//         'date_from'=> Carbon::now(),
//         'date_to'=> Carbon::now(),
//       'remember_token' => Str::random(50),  
//  ];
// });

$factory->define(Leave_total::class,function(Faker $faker){

return [
        'emp_id'=> random_int(1, 50),
       // 'schedule_id'=> random_int(1, 2),
        'total_leave'=> 25,
        'remaning_leave'=> 25,
      'remember_token' => Str::random(50),  
 ];
});


