<?php

use Illuminate\Database\Seeder;

use App\Models\Employee;
use App\Models\Schedule;
use App\Models\Shifts;
use App\Models\Roster;
class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Employee::create([
    		'name'=>'admin' ,
    		'phone'=>'01920164553',
    		'email'=>'admin@gmail.com',
    		'username'=>'admin',
    		'password'=>bcrypt('123456') ,
    		'sex'=>'male',
    		'department'=>'hr',
            'designation'=>'admin'
     ]);
        
            factory(\App\Models\Employee::class, 50)->create();

    }
}
