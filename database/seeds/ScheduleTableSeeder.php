<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Schedule;
use App\Models\Shifts;
class ScheduleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
 		Schedule::insert([
            'timestart' => '9:00',
            'timeend' => '17:00'
        ]);

        Schedule::insert([
            'timestart' => '18:00',
            'timeend' => '2:00'
        ]);

    }
    
}
