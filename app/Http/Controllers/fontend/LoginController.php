<?php

namespace App\Http\Controllers\fontend;

//use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Models\Employee;

class LoginController extends Controller
{

	public function create()
	
	{
	
		return view('login');

	}

	public function login(Request $request)
	{

		$request->validate([
			'username' => 'required',
			'password' => 'required',

		]);


		$credentials = $request->only('username','password',);
			
        if (Auth::attempt($credentials)) {

            // Authentication passed...

         return redirect()->route('home')->with('message','Login successful');
        
        }

        else{ 

        	$this->setErrormsg('Invalid Username OR Password') ; 

        	return redirect()->route('login');		
        }	


	}
	public function logout()
	{
		// dd("test");
		Auth::logout();
		session()->flush();
		return redirect()->back();
	}



}