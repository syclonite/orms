<?php

namespace App\Http\Controllers\fontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Leave_total;
use App\Models\Leave_taken;


class TotalLeaveController extends Controller
{
	public function create()
	{	

		$total_leave = Leave_total::get();

		return view('leave',compact('total_leave'));

	}



	public function leavecalc($id)

	{	



		$request_leave = Leave_Taken::find($id);
		
		$remaning_leave = Leave_total::select('remaning_leave')->where('emp_id',$request_leave->emp_id)->first();

		// dd($remaning_leave);

		$current_leave=($remaning_leave->remaning_leave-$request_leave->duration);
		
		$taken = Leave_total::where('emp_id',$request_leave->emp_id)->update([
		'remaning_leave'=>$current_leave
			]);

		$update = Leave_taken::where('emp_id',$request_leave->emp_id)->update(['status'=> 'appoved' ,'approved_by'=>auth()->user()->name ]);

		return redirect()->back();


	}

	public function leavereject($id)
	{


		$request_leave = Leave_Taken::find($id);
		
		$remaning_leave = Leave_total::select('remaning_leave')->where('emp_id',$request_leave->emp_id)->first();

           $reject = Leave_taken::where('emp_id',$request_leave->emp_id)->update(['status'=> 'rejected' ,'approved_by'=>auth()->user()->name ]);

		return redirect()->back();

	}


}
