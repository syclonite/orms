<?php

namespace App\Http\Controllers\fontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Holiday;
use App\Models\Shifts;
use App\Models\Schedule;
use App\Models\Roster;
use App\Models\Roster_details;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Datetime;
use DateInterval;
use DatePeriod;


class RosterDetailsController extends Controller
{


	public function create(Request $request)
	{	

		$employees = Employee::all();
		$shifts = Shifts::all();
		$rosters = Roster::all();
		$roster_details = Roster_details::all();

		return view('roster',compact('shifts','employees','roster_details','rosters'));


	}


	public function store( Request $request)
	{	

		//dd($request->all());
		$this->validate($request,[
			'date_from'=>'required',
			'date_to'=>'required',
			'shift_id'=>'required',
			'emp_id'=>'required',
			
		]);


		$roster_details = $request->input('emp_id');

		$begin = $request->input('date_from');
		$end = $request->input('date_to');	

		$start = DateTime::createFromFormat('Y-m-d', $begin);
		$finish = DateTime::createFromFormat('Y-m-d', 	$end);	


		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($start, $interval ,$finish);

		$roster=Roster::create([
			'date_from'=>$start->format("Y-m-d"),
			'date_to'=>$finish->format("Y-m-d"),
			'shift_id'=>$request->input('shift_id')
		]);

		try{	


			foreach($daterange as $dr) {
				if(is_array($roster_details))
				{ 
					foreach($roster_details as $data)
					{		
						Roster_details::create([
							'roster_id'=> $roster->id,
							'shift_id'=> $roster->shift_id,
							'date' => $dr->format("Y-m-d"),
							'emp_id' => $data, 
						]);


					}

				}else{

					echo "Not an array";

				}			


			}
			$this->setSuccessmsg('Roster Created');

		}
		catch(Exception $e) {

//	session()->flash('message',$e->getMessage());

//session()->flash('type','Danger');

			$this->setErrormsg($e->getMessage());


		}

		return redirect()->back();


	}


	public function adminview($id)
	{

	$roster_details = Roster_details::where('roster_id',$id)->get();

		$shifts=Shifts::all();

		return view('adminpanel_view', compact('roster_details','shifts'));

	}


	public function myroster(Request $request)

	{
		$holidays = Holiday::get();

		if ($request->input('date_from') && $request->input('date_to'))

		{    
			$start = date("Y-m-d",strtotime($request->input('date_from')));
			$end = date("Y-m-d",strtotime($request->input('date_to')));

			$roster_details = Roster_details::whereBetween('date',[$start,$end])->where('emp_id',auth()->user()->id)->orderBy('date','ASC')->get();	


			return view('myroster',compact('roster_details','holidays'));

		}

		$roster_details = Roster_details::where('emp_id',auth()->user()->id)->orderBy('date','DESC')->get();
		
		return view('myroster',compact('roster_details','holidays'));

	}



	public function empviewroster()
	{	

		$employees = Employee::all();	
		$roster_details = Roster_details::select('roster_id','emp_id','date')->get();

		$holidays = Holiday::get();
		//dd($holidays);

		return view('employeeviewroster',compact('roster_details','employees','holidays'));


	}




	public function deleteRoster($id)

	{

		Roster::find($id)->delete();
		return redirect()->back();


	}


	public function updateRosterDetails(Request $request,$id)

	{

		$data=[
			'shift_id'=>$request->input('shift_id')
			
			];

		$update = Roster_details::where('id',$id)->update($data);
		return redirect()->back();	
		


	}


	public function deleteRosterDetails($id)

	{
		Roster_details::find($id)->delete();
		return redirect()->back();

	}


}
