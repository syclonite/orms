<?php

namespace App\Http\Controllers\fontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shifts;
use App\Models\Schedule;
use Carbon\Carbon;


class ShiftController extends Controller
{

	public function show(){

		$data= Shifts::all();

		return view('shift',compact('data'));

	}

	public function store(Request $request){

		$this->validate($request,[
			'shift_name'=>'required',
			'time_start'=>'required',
			'time_end'=>'required',
		]);

		$data=[

			'timestart'=>trim($request->input('time_start')),
			'timeend'=>trim($request->input('time_end')),

		];

		try{

			$schedule=Schedule::create($data);

			$shift=Shifts::create([

				'schedule_id'=>$schedule->id,
				'name'=>$request->input('shift_name'),


			]);

			$this->setSuccessmsg('Shift Created');


			return redirect()->back();

		}

		catch(Exception $e){


			$this->setErrormsg($e->getMessage());


			return redirect()->back(); 

		}



	}



	public function update(Request $request,$id){


		$data= [
			'timestart'=>$request->input('time_start') ,
			'timeend'=>trim($request->input('time_end')),
									
		];

		$update = Schedule::where('id',$id)->update($data);


		$shift= [

		'name'=>trim($request->input('shift_name')),
									
		];


		$shift_update = Shifts::where('id',$id)->update($shift);


		return redirect()->back();	

	}



		public function destroy($id)

		{

		Shifts::find($id)->delete();

		Schedule::find($id)->delete();


		return redirect()->back();
	}





}
