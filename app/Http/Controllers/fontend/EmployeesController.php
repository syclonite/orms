<?php

namespace App\Http\Controllers\fontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Leave_total;


class EmployeesController extends Controller
{
	public function create()


	{
		return view('register');

	}


	public function employeepanel()

	{
		$data = Employee::where('id',auth()->user()->id)->get();

		return view('employeepanel',compact('data'));

	} 

	public function update_employeepanel(Request $request)

	{
		$data= [
			'name'=>$request->input('name') ,
			'phone'=>trim($request->input('phone')),
			'email'=>trim($request->input('email')),
			'username'=>trim($request->input('username')),
			'password'=>trim(bcrypt($request->input('password'))),
			'present'=>$request->input('present'),
			'permanent'=>$request->input('present'),
			'fname'=>$request->input('fname'),
			'mname'=>$request->input('mname'),
			];

			try{

			Employee::where('id',auth()->user()->id)->update($data);

			$this->setSuccessmsg('User updated');

			return redirect()->back();
		}
		catch(Exception $e) {
			$this->setErrormsg($e->getMessage());
			return redirect()->back(); 
		}


	} 

	


	public function store(Request $request)

	{

		$this->validate($request,[
			'name'=>'required',
			'phone'=>'required|max:11',
			'email'=>'required|email',
			'username'=>'required',
			'password'=>'required',
			'department'=>'required',
			'sex'=>'required',
			'designation'=>'required',
			'fname'=>'required',
			'mname'=>'required',
			'present'=>'required',
			'permanent'=>'required',
			'ssc'=>'required',
			'hsc'=>'required',
			'bsc'=>'required',
			// 'photo'=>'required|image|max:50000',
		]);

// dd($request->file('photo')); //var_dump() & die()

		// $photo = $request->file('photo');
		// $file_name = uniqid('photo', true).str_random(10).'.'.$photo->getClientOriginalExtension();

		// if($photo->isValid())
		// {

		// 	$photo->storeAs('images', $file_name);

		// }


		$data= [
			'name'=>$request->input('name') ,
			'phone'=>trim($request->input('phone')),
			'email'=>trim($request->input('email')),
			'username'=>trim($request->input('username')),
			'password'=>trim(bcrypt($request->input('password'))),
			'department'=>$request->input('department'),
			'sex'=>$request->input('sex'),
			'present'=>$request->input('present'),
			'permanent'=>$request->input('present'),
			'fname'=>$request->input('fname'),
			'mname'=>$request->input('mname'),
			'ssc'=>$request->input('ssc'),
			'hsc'=>$request->input('hsc'),
			'bsc'=>$request->input('bsc'),
			'designation'=>$request->input('designation'),
		];

		try{


			$employee=Employee::create($data);
			$leave_total=Leave_total::insert([


				'emp_id'=>$employee->id,

				'total_leave'=>25,  

				'remaning_leave' =>25,


			]);

			$this->setSuccessmsg('User Created');


			return redirect()->back();

		}
		catch(Exception $e) {

//	session()->flash('message',$e->getMessage());

//session()->flash('type','Danger');

			$this->setErrormsg($e->getMessage());


			return redirect()->back(); 
		}

		

	}


	public function showEdit($id)
	{
		$employee = Employee::find($id);
		return view('employeeDetails', compact('employee'));

	}

	public function showall()

	{


		$employees = Employee::get();

		return view('employeeprofile',compact('employees'));

	}

	public function update(Request $request, $id)
	{

		//dd($request->all());
		$data= [
			'name'=>$request->input('name') ,
			'phone'=>trim($request->input('phone')),
			'email'=>trim($request->input('email')),
			'username'=>trim($request->input('username')),
			//'password'=>trim(bcrypt($request->input('password'))),
			'department'=>$request->input('department'),
			'designation'=>$request->input('designation'),
		];

		try{

			Employee::where('id', $id)->update($data);

			$this->setSuccessmsg('User updated');

			return redirect()->back();
		}
		catch(Exception $e) {
			$this->setErrormsg($e->getMessage());
			return redirect()->back(); 
		}

	}

	public function destroy($id){

		Employee::find($id)->delete();
        //$article = Article::find($id);
		return redirect()->back();
	}



}