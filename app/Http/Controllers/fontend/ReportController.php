<?php

namespace App\Http\Controllers\fontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Roster_details;

class ReportController extends Controller
{
    

    public function show(Request $request){

//    	

		if ($request->input('date_from') && $request->input('date_to'))

		{    
			$start = date("Y-m-d",strtotime($request->input('date_from')));
			$end = date("Y-m-d",strtotime($request->input('date_to')));

			$roster_details = Roster_details::whereBetween('date',[$start,$end])->orderBy('date','ASC')->get();	


			return view('report',compact('roster_details'));

		}

		

		$roster_details = Roster_details::get();
		
		return view('report',compact('roster_details'));


	}



    }




