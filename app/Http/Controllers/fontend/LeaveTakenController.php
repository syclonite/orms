<?php

namespace App\Http\Controllers\fontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Leave_taken;
use App\Models\Roster_details;
use App\Models\leave_total;
use Carbon\Carbon;
 
class LeaveTakenController extends Controller
{



	public function leaveapplication()
	{	

	$leave_application = Leave_taken::where('emp_id',auth()->user()->id)->paginate(10);
		return view('leaveApplication',compact('leave_application'));


	}

	public function storeapplication(Request $request)
	
	{	


			$this->validate($request,[
			'location'=>'required',
			'emg_contact'=>'required',
			'leave_type'=>'required',
			'date_from'=>'required|after:today',
			'date_to'=>'required|after:date_from',
			'reason'=>'required',
		]);


		//dd($request->all());
		$start=$request->input('date_from');
		$end=$request->input('date_to');

		$from = Carbon::createFromFormat('Y-m-d', $start);
		$to = Carbon::createFromFormat('Y-m-d', $end);
		$interval = $to->diffInDays($from);
		//dd($interval);
		
		$data = [
			'emp_id'=>auth()->user()->id,
			'location'=>$request->input('location'),
			'urgent_contact'=> $request->input('emg_contact'),
			'leave_type'=>$request->input('leave_type'),
			'date_from'=>$start,
			'date_to'=>$end,
			'duration'=>$interval,	
			'reason'=>$request->input('reason'),
			
		];
		
		try{

			$leave = Leave_taken::create($data);
			$this->setSuccessmsg('Leave Application Submit');

			return redirect()->back();

		}
		catch(Exception $e) {

//	session()->flash('message',$e->getMessage());

//session()->flash('type','Danger');

			$this->setErrormsg($e->getMessage());

		}
	}


	public function showapplication()

	{   

	
		$data = leave_taken::all();


// dd($data);
		return view('leaveapprovalform',compact('data'));

	}


	public function myleavestatus()
	{

		$data = Leave_taken::where('emp_id',auth()->user()->id)->orderBy('date_from','DESC')->get();
		return view('myleavestatus',compact('data'));

	}




}
