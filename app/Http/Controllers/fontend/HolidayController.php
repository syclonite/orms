<?php

namespace App\Http\Controllers\fontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Holiday;
use Datetime;
use DateInterval;
use DatePeriod;

class HolidayController extends Controller
{

	public function create()

	{	


		$holidays=Holiday::all();
		return view('holiday',compact('holidays'));

	}

	public function store(Request $request)
	{
		//dd($request->all());


		$begin = $request->input('date_from');
		$end = $request->input('date_to');	

		$start = DateTime::createFromFormat('Y-m-d', $begin);
		$finish = DateTime::createFromFormat('Y-m-d', 	$end);

		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($start, $interval ,$finish);
		
		foreach($daterange as $dr) 

		{			
					Holiday::create([
						'date'=> $dr->format('Y-m-d'),
						'holiday_name'=> $request->input('name'),
						'discription'=> $request->input('discription'),
						 
					]);
				}
							



		return redirect()->back();

	}


	public function update(Request $request,$id)

	{	
		//dd($request->all());


		$data= [
			'holiday_name'=>$request->input('name') ,
			'date'=>trim($request->input('date')),
			'discription'=>trim($request->input('discription')),
						
		];

		$update = Holiday::where('id',$id)->update($data);
		return redirect()->back();	

	}

		public function destroy($id){

		Holiday::find($id)->delete();
		return redirect()->back();
	}





}
