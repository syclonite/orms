<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


public function setSuccessmsg($message): void
{


session()->flash('message',$message);

session()->flash('type','success');

}


public function setErrormsg($message):void
{


session()->flash('message',$message);

session()->flash('type','danger');



}



}
