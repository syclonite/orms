<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Leave_taken extends Model
{
    

	protected $fillable = [ 

		'emp_id','date_from','date_to','location','urgent_contact','duration','urgent_contact'	,'leave_type','status','approved_by','reason',

	];



	public function employee()
	{

		return $this->belongsTo(Employee::class,'emp_id');


	}

	public function leave_total()

	{

			return $this->belongsTo(Leave_total::class,'emp_id');


	}


}
