<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{
	use Notifiable;
	
    protected $fillable = [

   		'name','phone','present','permanent','email','username','password','sex','department','designation','fname','mname','ssc','hsc','bsc',
   ];
}
