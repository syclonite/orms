<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leave_total extends Model
{
    
	protected $fillable= [

		'emp_id'

	];



	public function employee()
	{

		return $this->belongsTo(Employee::class,'emp_id');

	}

	public function leave_taken()
	{


		return $this->belongsTo(Leave_taken::class,'emp_id');

	}



}
