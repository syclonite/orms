<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roster_details extends Model
{
    
	protected $fillable =[


	'roster_id','emp_id','shift_id','date',
	
	];  



		public function roster()

		{

			return $this->belongsTo(Roster::class,'id','date_from','date_to');

		}

			public function employee()

			{

				return $this->belongsTo(Employee::class,'emp_id');


			}


			
		public function shift()

		{

			return $this->belongsTo(Shifts::class,'shift_id');


		}

		
			public function holiday()

		{

			return $this->belongsTo(Holiday::class,'id');


		}

		

    
}
